<?php

use Lambq\Websocket\Websocket;


if (!function_exists('websocket'))
{
    function websocket()
    {
        return new Websocket();
    }
}
