<?php

return [
    /*
    |--------------------------------------------------------------------------
    | Lambq-Mqtt install directory
    |--------------------------------------------------------------------------
    |
    | The installation directory of the controller and routing configuration
    | files of the administration page. The default is `app/Admin`, which must
    | be set before running `artisan admin::install` to take effect.
    |
    */
    'directory' => app_path('WebSocket'),
];